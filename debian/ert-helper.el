(ert-run-tests-batch-and-exit
 '(not (or
        ;; those tests fail in autopkgtest. they should be fixed, but
        ;; for now we feel it's better to have a partially working
        ;; autopkgtest than the package out of stable/testing.
        "rg-integration-test/project-root"
        "rg-integration/dwim-search"
        "rg-integration/project-search"
        "rg-integration/rg-executable-results-buffer"
        "rg-integration/rg-executable-through-rg-run"
        "rg-integration/rg-recompile"
        "rg-unit/isearch-project"
        )))
